package com.itau.anuncios.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.itau.anuncios.model.Oferta;
import com.itau.anuncios.repository.OfertaRepository;

@Controller
public class OfertaController {
	
	@Autowired
	OfertaRepository ofertarepository;
	
	// CADASTRAR Oferta
	
	@RequestMapping(path="/cadastrooferta", method=RequestMethod.POST)
	@ResponseBody
	public Oferta inserirOferta(@RequestBody Oferta oferta) {
		
		return ofertarepository.save(oferta);
	}
	
	// FIM CADASTRO Oferta
	
	//BUSCA LISTA DE Ofertas
	Oferta oferta = new Oferta();
	
	@RequestMapping(path="/Ofertas", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Oferta> getOfertaTodos() {		
		return ofertarepository.findAll();
	}
	//FIM BUSCA LISTA DE OfertaS
}
