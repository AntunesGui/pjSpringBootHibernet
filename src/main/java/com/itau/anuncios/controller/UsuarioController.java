package com.itau.anuncios.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncios.model.Anuncio;
import com.itau.anuncios.model.Usuario;
import com.itau.anuncios.repository.UsuarioRepository;
import com.itau.anuncios.service.PasswordService;
import com.itau.anuncios.service.TokenService;


@Controller
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	TokenService tokenService;
	

	// CADASTRAR USUARIO

	@RequestMapping(path="/cadastrousuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		String hash = passwordService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		
		return usuarioRepository.save(usuario);
	}

	// FIM CADASTRO USUARIO


	//Usuario usuario = new Usuario();

	//@RequestMapping(path="/usuario/{email}", method=RequestMethod.GET)
	//@ResponseBody
	//public Usuario getUsuarioByEmail(@PathVariable String email) {		
	//	return usuarioRepository.findByEmail(email);
	//}
	
	
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());
		
		if(! usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean deuCerto = passwordService.verificar(usuario.getSenha(), usuarioBanco.get().getSenha());
		
		if(deuCerto) {
			String token = tokenService.gerar(usuarioBanco.get().getId());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", token);
			
			
			System.out.println(token);
			
			return new ResponseEntity<Usuario>(usuarioBanco.get(), headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}
	
	@RequestMapping(path="/check", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> verificar(HttpServletRequest request) {
		String token = request.getHeader("Authorization").replace("Bearer ","");
		
		System.out.println(token);
		
		long id = tokenService.verificar(token);
		
		Optional<Usuario> usuarioBanco = usuarioRepository.findById(id);
		
		if(usuarioBanco.isPresent()) {
			return ResponseEntity.ok(usuarioBanco.get());
		}

		return ResponseEntity.badRequest().build();
	}
	
	
	

}