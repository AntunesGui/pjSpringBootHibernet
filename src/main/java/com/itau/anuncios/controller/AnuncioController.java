package com.itau.anuncios.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.assembler.InterfaceBasedMBeanInfoAssembler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.anuncios.model.Anuncio;
import com.itau.anuncios.model.Usuario;
import com.itau.anuncios.repository.AnuncioRepository;
import com.itau.anuncios.repository.UsuarioRepository;

@Controller
public class AnuncioController {

	@Autowired
	AnuncioRepository anuncioRepository;
	UsuarioRepository usuarioRepository;

	// CADASTRAR ANUNCIO

	@RequestMapping(path="/cadastroanuncio", method=RequestMethod.POST)
	@ResponseBody

	public Anuncio inserirAnuncio(@RequestBody Anuncio anuncio) {

		return anuncioRepository.save(anuncio);
	}

	// FIM CADASTRO ANUNCIO

	//BUSCA LISTA DE ANUNCIOS
	Anuncio anuncio = new Anuncio();

	@RequestMapping(path="/anuncios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Anuncio> getAnuncioTodos() {		
		return anuncioRepository.findAll();
	}
	//FIM BUSCA LISTA DE ANUNCIOS

	@RequestMapping(path="/usuarioteste")
	@ResponseBody
	public Anuncio teste() {
		Usuario usuario = new Usuario();
		usuario.setNome("Carla");

		Anuncio anuncio = new Anuncio();

		anuncio.setTitulo("TesteAnuncio");
		anuncio.setId(1);
		anuncio.setDataExpiracao(java.sql.Date.valueOf("2018-05-30"));

		Set<Anuncio> anuncios = new HashSet<Anuncio>();

		anuncios.add(anuncio);

		anuncio = anuncioRepository.save(anuncio);
		usuario.setAnuncios(anuncios);
		usuarioRepository.save(usuario);
		return anuncio;
	}
}
