package com.itau.anuncios.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.anuncios.model.Usuario;


public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	Optional<Usuario> findByEmail(String email);

}