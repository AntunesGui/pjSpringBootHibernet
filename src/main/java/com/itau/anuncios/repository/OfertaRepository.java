package com.itau.anuncios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.anuncios.model.Oferta;


public interface OfertaRepository extends CrudRepository<Oferta, Integer>{

}