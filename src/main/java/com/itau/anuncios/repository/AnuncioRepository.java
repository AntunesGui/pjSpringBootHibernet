package com.itau.anuncios.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.anuncios.model.Anuncio;


public interface AnuncioRepository extends CrudRepository<Anuncio, Integer>{

	}