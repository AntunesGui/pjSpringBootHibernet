package com.itau.anuncios.service;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

@Service
public class TokenService {
	String secret = "carla-gui123";
	
	public String gerar(long id) {
		try {
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    return JWT.create().withClaim("idUsuario", id).sign(algorithm);
		} catch (Exception exception){
			exception.printStackTrace();
			
			return null;
		}
	}
	
	
	public long verificar(String token) {
		try {
		    Algorithm algorithm = Algorithm.HMAC256(secret);
		    JWTVerifier verifier = JWT.require(algorithm).build();
		    DecodedJWT jwt = verifier.verify(token);
		    return jwt.getClaim("idUsuario").asLong();
		} catch (Exception exception){
		    exception.printStackTrace();
			
			return 0;
		}
	}
}